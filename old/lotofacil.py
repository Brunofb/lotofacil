#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#       lotofacil.py - Script para conferir resultados da Lotofácil (Loteria Brasileira)
#
#       Copyright 2011 Thomas Jefferson Pereira Lopes <thomas@thlopes.com>
#       Parte integrante do projeto PyLottery (https://bitbucket.org/THLopes/pylottery)
#
#       Uso: python lotofacil.py [ARQUIVO] [CONCURSO] [RESULTADO]
#         CONCURSO = número do concurso a conferir, por exemplo, 1245
#         ARQUIVO = nome do arquivo com os jogos, na seguinte sintaxe:
#           [nomedojogador1,670,674]
#           1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
#           1,3,5,7,9,10,12,14,16,18,19,20,21,23,25
#           
#           [nomedojogador2,673,681]
#           2,3,6,7,9,11,13,15,17,18,19,20,22,24,25
#
#           [nomedojogador3]
#           3,6,7,9,11,13,15,17,18,19,20,21,22,23,24
#           ...
#         RESULTADO = Coloque o resultado manual caso queira conferir algum
#            sorteio que ainda nao foi publicado no site da caixa, com os numeros
#            separados por virgula, sem espacos. Ex.: 1,7,22,31,40,58
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.

import sys
from BeautifulSoup import BeautifulSoup
import urllib2
import cookielib

# user agent para funcionar como um browser comum
user_agent = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.11 (KHTML, like Gecko) Ubuntu/11.04 Chromium/17.0.963.79 Chrome/17.0.963.79 Safari/535.11"

# cookiejar para tratar corretamente os cookies enviados pelo servidor
cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
# Dica do WagnerLuis1982, presente no gval (https://github.com/wagnerluis1982/gval/blob/master/src/gval/util.py)
opener.addheaders.append(("Cookie", "security=true"))
urllib2.install_opener(opener)

def checa_jogo(sorteio, jogo):
    acertos = 0
    for numero in sorteio:
        if (numero in jogo):
            acertos += 1
    return acertos

def main():
    concurso = None
    nome_arquivo = 'lotofacil.txt'
    resultado_manual = None
    if (len(sys.argv) > 1):
        nome_arquivo = sys.argv[1]
    if (len(sys.argv) > 2):
        concurso = sys.argv[2]
    if (len(sys.argv) > 3):
        resultado_manual = sys.argv[3]
    
    # checar arquivo
    try:
        arquivo_jogos = open(nome_arquivo, 'r')
    except:
        print "ERRO: Arquivo não encontrado!!!"
        sys.exit(1)
    
    if not resultado_manual:
        if (concurso):
            search_msg = "Buscando resultado do concurso %s..." % (concurso)
            urlcaixa = "http://www1.caixa.gov.br/loterias/loterias/lotofacil/lotofacil_pesquisa_new.asp?submeteu=sim&opcao=concurso&txtConcurso=%s" % (concurso)
            last = False 
        else:
            search_msg = "Buscando resultado do último concurso..."
            urlcaixa = "http://www1.caixa.gov.br/loterias/loterias/lotofacil/lotofacil_pesquisa_new.asp?app=1296579402701"
            last = True
    
        print search_msg
        
        h = { 'User-Agent' : user_agent }
        req = urllib2.Request(urlcaixa, headers=h)
        response = urllib2.urlopen(req)
        html = response.read()
        
        if last:
            ultimo_concurso = html.split('|')[0]
            print "Concurso %s" % ultimo_concurso
        else:
            ultimo_concurso = concurso
            
        soup = BeautifulSoup(html)
        
        is_last = not 'Ver pr' in soup.text
    
        # Debug, será removido em breve
        #print urlcaixa
        #print last, is_last, 'Ver pr' in soup.text
        #print soup.text
        
        if '|N' in soup.text and 'o existe resul' in soup.text:
            print "ERRO: Sorteio não realizado/localizado!!!"
            sys.exit(1)
        
        if last or is_last:
            numeros_sorteados = map(int, soup.text.replace('|||','||').split('||')[1].split('|')[0:15])
        else: 
            numeros_sorteados = map(int, soup.text.replace('|||','||').split('||')[0].split('|')[3:18])
        
        if (len(numeros_sorteados) == 0):
            print "ERRO: Sorteio não localizado!!!"
            sys.exit(1)
        
        jogo_sorteado = numeros_sorteados

    else:
        ultimo_concurso = concurso
        print "Utilizando resultado manual informado, concurso %s" % ultimo_concurso 
        jogo_sorteado = [int(num) for num in resultado_manual.split(',')]
    print "Números sorteados: %s" % (jogo_sorteado)

    # lendo jogos do arquivo
    linhas = arquivo_jogos.readlines()
    counter = 1
    print "Conferindo Jogos"
    
    total_premiados = 0
    conferir_proximo = True
    
    for linha in linhas:
        if len(linha) < 2:
            pass
        elif "[" in linha or "]" in linha:
            # linha de rotulo e jogos
            rotulo_bloco = linha.strip().replace('[','').replace(']','').split(',')
            try:
                jogo_inicial = int(rotulo_bloco[1])
            except IndexError:
                jogo_inicial = 0
            try:
                jogo_final = int(rotulo_bloco[2])
            except IndexError:
                if jogo_inicial == 0:
                    jogo_final = int(ultimo_concurso)
                else:
                    jogo_final = jogo_inicial
                    
            concurso_atual = int(ultimo_concurso)
            if concurso_atual >= jogo_inicial and concurso_atual <= jogo_final:
                conferir_proximo = True
                print "================"
                print "%s (%s a %s)" % (rotulo_bloco[0], jogo_inicial, jogo_final) # Nome do Jogador
            else:
                conferir_proximo = False
            counter = 1
        else:
            if conferir_proximo:
                jogo = map(int,linha.split(','))
                resultado = checa_jogo(jogo_sorteado, jogo)
                if (resultado > 10):
                        print "\nAcertos: ***  %s *** - Jogo %s: %s\n" % (resultado,counter,jogo)
                        total_premiados += 1
                else:
                    print "Acertos: %s - Jogo %s: %s" % (resultado,counter,jogo)
                counter += 1
    
    print "================"
    print "Total de Jogos premiados: %s" % (total_premiados)

if (__name__ == '__main__'):
    main()
