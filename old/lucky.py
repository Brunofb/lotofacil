#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       lucky.py - Script para gerar numeros aleatórios para as Loterias Brasileiras
#
#       Copyright 2011 Thomas Jefferson Pereira Lopes <thomas@thlopes.com>
#
#       Uso: python lucky.py [MAXIMO] [QUANTIDADE] [JOGOS] [AMOSTRA]
#         MAXIMO = Último número que poderá ser sorteado (inclusive). Default: 60
#         QUANTIDADE = Quantidade de números que serão sorteados por jogo. Default: 6
#         JOGOS = Quantidade de jogos que serão sorteados para montar o lote. Default: 1
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.

import sys
import random

def sortear(maximo=60, quantidade=6, amostra=None):
    if not amostra:
        amostra = xrange(1,maximo+1)
    if quantidade > len(amostra):
        quantidade = len(amostra)
        
    sorteados = random.sample(amostra, quantidade)
    sorteados.sort()
    return sorteados

def main():
    maximo = 60
    quantidade = 6
    jogos = 1
    amostra = None
    
    if (len(sys.argv) > 1):
        maximo = int(sys.argv[1])
    if (len(sys.argv) > 2):
        quantidade = int(sys.argv[2])
    if (len(sys.argv) > 3):
        jogos = int(sys.argv[3])
    if (len(sys.argv) > 4):
        amostra = map(int, sys.argv[4].split(','))
    
    i = 0
    while i < jogos:
        print ",".join(map(str, sortear(maximo, quantidade, amostra)))
        i += 1
    
if (__name__ == '__main__'):
    main()
