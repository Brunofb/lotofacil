#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#       megasena.py - Script para conferir resultados da Mega Sena (Loteria Brasileira)
#
#       Copyright 2010 Thomas Jefferson Pereira Lopes <thomas@thlopes.com>
#
#       Uso: python megasena.py [ARQUIVO] [CONCURSO] [RESULTADO]
#         CONCURSO = número do concurso a conferir, por exemplo, 1245
#         ARQUIVO = nome do arquivo com os jogos, na seguinte sintaxe:
#           [nomedojogador1,1245,1253]
#           1,2,3,4,5,6
#           4,19,34,48,53
#
#           [nomedojogador2]
#           5,20,35,49,54
#           ...
#         RESULTADO = Coloque o resultado manual caso queira conferir algum
#            sorteio que ainda nao foi publicado no site da caixa, com os numeros
#            separados por virgula, sem espacos. Ex.: 1,7,22,31,40,58
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.

import sys
from BeautifulSoup import BeautifulSoup
import urllib2
import cookielib
import shelve

# user agent para funcionar como um browser comum
user_agent = "Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.11 (KHTML, like Gecko) Ubuntu/11.04 Chromium/17.0.963.79 Chrome/17.0.963.79 Safari/535.11"

# cookiejar para tratar corretamente os cookies enviados pelo servidor
cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
urllib2.install_opener(opener)

def checa_cache(concurso, loteria='megasena'):
    concurso = str(concurso)
    loteria = str(loteria)
    resultados_cache = shelve.open(".pylottery_cache")
    if resultados_cache.has_key(loteria):
        resultados_concursos = resultados_cache[loteria]
        if resultados_concursos.has_key(concurso):
            return  resultados_concursos[concurso]
    return False

def atualiza_cache(concurso, resultado, loteria='megasena'):
    concurso = str(concurso)
    loteria = str(loteria)
    resultados_concursos = {}
    resultados_cache = shelve.open(".pylottery_cache")
    if resultados_cache.has_key(loteria):
        resultados_concursos = resultados_cache[loteria]
    # TODO: this function always return True, but it's not supposed to be this way
    resultados_concursos[concurso] = resultado
    resultados_cache[loteria] = resultados_concursos
    resultados_cache.close() 
    return True

def ultimo_concurso_cache(loteria='megasena'):
    loteria = str(loteria)
    resultados_concursos = {}
    resultados_cache = shelve.open(".pylottery_cache")
    if resultados_cache.has_key(loteria):
        resultados_concursos = resultados_cache[loteria]
        resultados_concursos = resultados_concursos.items()
        resultados_concursos.sort() 
        return resultados_concursos[-1]
    #resultados_cache.close()
    return False
    

def busca_resultado(concurso=None):
    concurso
    if (concurso):
        search_msg = "Buscando resultado do concurso %s" % (concurso)
        urlcaixa = "http://www1.caixa.gov.br/loterias/loterias/megasena/megasena_pesquisa_new.asp?submeteu=sim&opcao=concurso&txtConcurso=%s" % (concurso)
        last = False 
    else:
        search_msg = "Buscando resultado do último concurso"
        urlcaixa = "http://www1.caixa.gov.br/loterias/loterias/megasena/megasena_pesquisa_new.asp?app=1296579402701"
        last = True

    print search_msg
    
    h = { 'User-Agent' : user_agent }
    req = urllib2.Request(urlcaixa, headers=h)
    response = urllib2.urlopen(req)
    html = response.read()
    
    if last:
        concurso = html.split('|')[0]
        print "DEBUG: Concurso %s" % concurso
     
    soup = BeautifulSoup(html)    
    
    if '|N' in soup.text and 'o existe resul' in soup.text:
        print "ERRO: Sorteio não realizado/localizado!!!"
        sys.exit(1)
    
    numeros_sorteados = soup.findAll('span','num_sorteio')
    
    if (len(numeros_sorteados) == 0):
        print "ERRO: Sorteio não localizado!!!"
        return None
    
    numeros_sorteados = numeros_sorteados[1].findAll('li')
    jogo_sorteado = [int(num.string) for num in numeros_sorteados]
    
    return jogo_sorteado, concurso

def checa_jogo(sorteio, jogo):
    acertos = 0
    for numero in sorteio:
        if (numero in jogo):
            acertos += 1
    return acertos

def main():
    concurso = None
    ultimo_concurso = None
    nome_arquivo = 'megasena.txt'
    resultado_manual = None
    if (len(sys.argv) > 1):
        nome_arquivo = sys.argv[1]
    if (len(sys.argv) > 2):
        concurso = sys.argv[2]
    if (len(sys.argv) > 3):
        resultado_manual = sys.argv[3]
    
    # checar arquivo
    try:
        arquivo_jogos = open(nome_arquivo, 'r')
    except:
        print "ERRO: Arquivo não encontrado!!!"
        sys.exit(1)
    
    if resultado_manual:
        ultimo_concurso = concurso
        print "Utilizando resultado manual informado, concurso %s" % ultimo_concurso 
        jogo_sorteado = [int(num) for num in resultado_manual.split(',')]
    else:
        
        # first, theres concourse number?
        if not concurso:
            jogo_sorteado, concurso = busca_resultado(concurso)
            print "DEBUG: Concurso: %s Resultado: %s" % (concurso, jogo_sorteado)
            ultimo_concurso = concurso            
        else:
            # first, check cache
            jogo_sorteado = checa_cache(concurso)
        
            if not jogo_sorteado:
                # no cache? Check online
                jogo_sorteado, concurso = busca_resultado(concurso)
            
            ultimo_concurso = ultimo_concurso_cache()[0]
        
        # update cache
        atualiza_cache(concurso, jogo_sorteado)
    
    print "Números sorteados: %s" % (jogo_sorteado)    

    # lendo jogos do arquivo
    linhas = arquivo_jogos.readlines()
    counter = 1
    print "Conferindo Jogos"
    
    total_premiados = 0
    conferir_proximo = True
    
    for linha in linhas:
        if len(linha) < 2:
            pass
        elif "[" in linha or "]" in linha:
            # linha de rotulo e jogos
            rotulo_bloco = linha.strip().replace('[','').replace(']','').split(',')
            try:
                jogo_inicial = int(rotulo_bloco[1])
            except IndexError:
                jogo_inicial = 0
            try:
                jogo_final = int(rotulo_bloco[2])
            except IndexError:
                if jogo_inicial == 0:
                    jogo_final = int(ultimo_concurso)
                else:
                    jogo_final = jogo_inicial
                
            concurso_atual = int(ultimo_concurso)
            if concurso_atual >= jogo_inicial and concurso_atual <= jogo_final:
                conferir_proximo = True
                print "================"
                print "%s (%s a %s)" % (rotulo_bloco[0], jogo_inicial, jogo_final) # Nome do Jogador
            else:
                conferir_proximo = False
            counter = 1
        else:
            if conferir_proximo:
                jogo = map(int,linha.split(','))
                resultado = checa_jogo(jogo_sorteado, jogo)
                if (resultado > 3):
                        print "\nAcertos: ***  %s *** - Jogo %s: %s\n" % (resultado,counter,jogo)
                        total_premiados += 1
                else:
                    print "Acertos: %s - Jogo %s: %s" % (resultado,counter,jogo)
                counter += 1
    
    print "================"
    print "Total de Jogos premiados: %s" % (total_premiados)

if (__name__ == '__main__'):
    main()

