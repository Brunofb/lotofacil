#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#       lotomania.py - Script para conferir resultados da Lotomania (Loteria Brasileira)
#
#       Copyright 2014 Thomas Jefferson Pereira Lopes <thomas@thlopes.com>
#       Parte integrante do projeto PyLottery (https://bitbucket.org/THLopes/pylottery)
#
#       Uso: python lotomania.py [ARQUIVO] [CONCURSO] [RESULTADO]
#         CONCURSO = número do concurso a conferir, por exemplo, 1245
#         ARQUIVO = nome do arquivo com os jogos, na seguinte sintaxe:
#           [nomedojogador1,670,674]
#           1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
#           1,3,5,7,9,10,12,14,16,18,19,20,21,23,25
#           
#           [nomedojogador2,673,681]
#           2,3,6,7,9,11,13,15,17,18,19,20,22,24,25
#
#           [nomedojogador3]
#           3,6,7,9,11,13,15,17,18,19,20,21,22,23,24
#           ...
#         RESULTADO = Coloque o resultado manual caso queira conferir algum
#            sorteio que ainda nao foi publicado no site da caixa, com os numeros
#            separados por virgula, sem espacos. Ex.: 1,7,22,31,40,58
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.

import sys
import re

from base import LoteriaBase, red, bold

class Lotomania(LoteriaBase):
    """ Classe para processar a Lotomania, herdada de LoteriaBase """
    def __init__(self, mensagens=True, cache=True):
        super(Lotomania, self).__init__(name="lotomania",
                              total_dezenas=100,
                              min_acertos= 16,
                              max_acertos = 100,    # serao 20 numeros sorteados, porem, usaremos 100 para identificar 00
                              arquivo="lotomania.txt",
                              url_base="http://www1.caixa.gov.br/loterias/loterias/lotomania/_lotomania_pesquisa.asp",
                              intervalo_medio=2,
                              mensagens=mensagens,
                              cache=cache)
    
    def extrair_sorteio(self, html):
        sorteios = re.findall(r"(?:\|\d{2}\|*?){20}", html)
        
        if sorteios:
            # vamos pegar os números em ordem de sorteio
            sorteados = sorteios[1]
            # a Lotomania possui 00, o que será traduzido aqui para 100
            sorteados = sorteados.replace('00', '100')
            sorteados = sorteados.split('|')
            if sorteados[0] == "":
                sorteados = sorteados[1:]
            numeros_sorteados = map(int, sorteados)
            if (len(numeros_sorteados) == 20):
                self._log(bold("Números sorteados: ") + "%s" % numeros_sorteados)
                return numeros_sorteados
            
        self._log(red("ERRO:",bold=True) + red(" Sorteio não localizado!!!"))
        return  None
    
    def extrair_data(self, html, index=0):
        """ Método para extrair a data do sorteio específico da Lotomania """
        return super(Lotomania, self).extrair_data(html, index)
    
    def checa_jogo(self, sorteio, jogo):
        """ Sobrescreve o método checa_jogo para que troque 0 acertos por 100 """
        acertos = super(Lotomania, self).checa_jogo(sorteio, jogo)
        if acertos == 0:
            acertos = 100
        return acertos


def main():
    concurso = None
    resultado_manual = None
    
    if (len(sys.argv) > 1):
        concurso = int(sys.argv[1])
    if (len(sys.argv) > 2):
        resultado_manual = sys.argv[2]
    
    lotomania = Lotomania()
    total_premios = 0
    linhas_premiadas = []
    
    total_premios, linhas_premiadas = lotomania.conferir(concurso, resultado=resultado_manual)
    
if (__name__ == '__main__'):
    main()
