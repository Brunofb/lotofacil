#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
#       quina.py - Script para conferir resultados da Quina (Loteria Brasileira)
#
#       Copyright 2013 Thomas Jefferson Pereira Lopes <thomas@thlopes.com>
#       Parte integrante do projeto PyLottery (https://bitbucket.org/THLopes/pylottery)
#
#       # TODO: Atualizar essa documentação conforme a nova loteria 
#       Uso: python quina.py [ARQUIVO] [CONCURSO] [RESULTADO]
#         CONCURSO = número do concurso a conferir, por exemplo, 1245
#         ARQUIVO = nome do arquivo com os jogos, na seguinte sintaxe:
#           [nomedojogador1,670,674]
#           1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
#           1,3,5,7,9,10,12,14,16,18,19,20,21,23,25
#           
#           [nomedojogador2,673,681]
#           2,3,6,7,9,11,13,15,17,18,19,20,22,24,25
#
#           [nomedojogador3]
#           3,6,7,9,11,13,15,17,18,19,20,21,22,23,24
#           ...
#         RESULTADO = Coloque o resultado manual caso queira conferir algum
#            sorteio que ainda nao foi publicado no site da caixa, com os numeros
#            separados por virgula, sem espacos. Ex.: 1,7,22,31,40,58
#       
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.

import sys
import re
from base import LoteriaBase, bold

class Quina(LoteriaBase):
    """ Classe para processar a Quina, herdada de LoteriaBase """
    def __init__(self, mensagens=True, cache=True):
        super(Quina, self).__init__(name="quina",
                              total_dezenas=80,
                              min_acertos=3,
                              max_acertos=5,
                              arquivo="quina.txt",
                              url_base="http://www1.caixa.gov.br/loterias/loterias/quina/quina_pesquisa_new.asp",
                              intervalo_medio=1,
                              mensagens=mensagens,
                              cache=cache)

    def extrair_sorteio(self, html):
        """ Método para extrair os números do sorteio específico da Quina """
        sorteio_match = re.search("<ul><li>(\d{2})</li><li>(\d{2})</li><li>(\d{2})</li><li>(\d{2})</li><li>(\d{2})</li></ul>", html)
        
        if sorteio_match:
            grupo_sorteio = sorteio_match.groups()
            numeros_sorteados = map(int, grupo_sorteio)
            if (len(numeros_sorteados) == self.max_acertos):
                self._log("Números sorteados: %s" % numeros_sorteados) 
                return numeros_sorteados
            
        self._log("ERRO: Sorteio não localizado!!!")
        return  None

    def extrair_data(self, html, index=0):
        data_concurso = None
        data_match = re.findall('\d{2}/\d{2}/\d{4}', html)
        data_concurso = data_match[index]
        self._log(bold("Data: ") + data_concurso)
        return data_concurso

        
def main():
    concurso = None
    resultado_manual = None
    
    if (len(sys.argv) > 1):
        concurso = int(sys.argv[1])
    if (len(sys.argv) > 2):
        resultado_manual = sys.argv[2]
    
    quina = Quina()
    total_premios = 0
    linhas_premiadas = []
    
    total_premios, linhas_premiadas = quina.conferir(concurso, resultado=resultado_manual)
    
if (__name__ == '__main__'):
    main()
