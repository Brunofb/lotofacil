#!/usr/bin/env python
# -*- coding: utf-8 -*-

from lotofacil import Lotofacil

def confere():
    concurso = None
    lotofacil = Lotofacil()
    premios = 0
    linhas = []

    for i in xrange(1000):
        premios, linhas, acumulado = lotofacil.conferir(i, resultado=None)
